# x86_64 linux is only supported platform ATM. 

#CC=musl-gcc -Wno-attributes #(see: musl-libc.org)
CC=clang 
CFLAGS=-g -I. -fpic
LINK=clang
LDFLAGS=-L. -fpic -nostdlib -shared 

SRCS=\
	std/unistd.c		\
	std/fcntl.c			\
	std/strlen.c		\
	std/strcpy.c		\
	std/itoa.c			\
	std/puts.c			\
	std/memset.c		\
	std/malloc.c		\
	std/base64.c		\
	std/reverse.c		\
	std/clump.c			\
	std/align.c			\
	std/debug.c			\
	os/spinlock.c		\


ASM=\
	os/syscall.x86_64.s\

OBJS=$(SRCS:.c=.o)
LIB=libg.so
BUILDDIR=build

.SUFFIXES: .c
.PHONY: prep all 

all: prep $(LIB) 

prep:
	@if [ ! -d $(BUILDDIR) ]; then mkdir $(BUILDDIR); fi

clean: 
	@find . -name *.o -exec rm {} \;
	@rm -r $(BUILDDIR);

.c.o: 
	$(CC) $(CFLAGS) -c $< -o $@ 


$(LIB): $(OBJS)  
	$(LINK) $(LDFLAGS) $(ASM) $(OBJS) -o $(BUILDDIR)/$(LIB) 
	
