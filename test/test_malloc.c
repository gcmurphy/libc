#include "std/stdio.h"
#include "std/malloc.h"
#include "std/string.h"

int main(void)
{
    char *buf = (char *) malloc(0xff);
    strcpy(buf, "This is a test");
    puts(buf);
    free(buf);
    return 0;
}
