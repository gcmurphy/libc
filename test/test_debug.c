
#include "std/debug.h"

int main(void){
    debugf("String %s\n", "1234");
    debugf("Decimal %d\n", 1234);
    debugf("Hex %x\n", 1234);
    debugf("Binary %b\n", 1234);
    return 0;
}
