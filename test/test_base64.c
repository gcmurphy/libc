
#include "std/malloc.h"
#include "std/stdio.h"
#include "std/string.h"
#include "std/utils.h"

int main(void)
{

  const char *input =  
    "Man is distinguished, not only by his reason, "
    "but by this singular passion from other animals, which is "
    "a lust of the mind, that by a perseverance of delight in "
    "the continued and indefatigable generation of knowledge, "
    "exceeds the short vehemence of any carnal pleasure.";

  const char *expected = 
      "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlz"
      "IHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2Yg"
      "dGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGlu"
      "dWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRo"
      "ZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";


    char *actual = base64_encode((void*)input, strlen(input));
    if (actual){ 
        puts("EXPECTED:");
        puts(expected);
        puts("ACTUAL: ");
        puts(actual);
        free(actual);
    }
    return 0;
}
