/*
 * Copyright (c) Grant Murphy 
 * All rights reserved
 */
.text
.globl _start
.type _start, @function
_start: 

    xorl    %ebp, %ebp
    popq    %rdi
    movq    %rsp, %rsi
    andq    $~15, %rsp

    callq   main@plt
    callq   quit@plt

