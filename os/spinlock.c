/*
 * Copyright (c) Grant Murphy 
 * All rights reserved. 
 */

#define LOCKED   1
#define UNLOCKED 0

void spinlock(volatile int *lock)
{
    while (__sync_lock_test_and_set(lock, LOCKED)){
        while (*lock);
    }
}

void spinunlock(volatile int *lock)
{
    __sync_lock_release(lock, UNLOCKED);
}
