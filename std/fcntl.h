#ifndef fcntl_h
#define fcntl_h

#include "std/types.h"

int creat(const char *path, mode_t mode);
int open(const char *path, int oflag, mode_t mode);
/* int fcntl(int fd, int cmd, ...); */

#endif /* fcnt_h */
