/* 
 * Copyright (c) Grant Murphy 
 * All rights reserved.
 */

#include "std/unistd.h"
#include "std/string.h"

void puts(const char *str)
{
    int len = strlen(str);
    const char *buf = str;
    if (len){
        write(1, buf, len);
        write(1, "\n", 1); 
    }

}
