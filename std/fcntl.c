/* 
 * Copyright (c) Grant Murphy 
 * All rights reserved.
 */

#include "std/types.h"
#include "os/syscall.h"

int open(const char *path, int oflag, mode_t mode){
    return syscall(syscall_open, path, oflag, mode);
}

int creat(const char *path, mode_t mode){
    return syscall(syscall_creat, path, mode);
}



