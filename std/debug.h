#ifndef debug_h
#define debug_h

#include "std/types.h"

struct blob_st {
    const unsigned char *data;
    size_t length;
};
typedef struct blob_st blob_t;

#define BLOB(b, s) &((blob_t){.data = b, .length = s})

void debugf(const char *fmt, ...);

#endif /* debug_h */
