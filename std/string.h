/* 
 * Copyright (c) Grant Murphy 
 * All rights reserved.
 */
#ifndef string_h
#define string_h

#include "std/types.h"

void *memset(void *buf, int n, size_t sz);
size_t strlen(const char*);
char * strcpy(char *dest, const char *src);

#endif  /* string_h */
