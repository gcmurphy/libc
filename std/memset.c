#include "std/types.h"

void* memset(void *buf, int v, size_t sz)
{
    char *p = (char *) buf;
    int i; 
    for (i = 0; i < sz; ++i){
        p[i] = v;
    }
    return buf;
}
