/* 
 * Copyright (c) Grant Murphy 
 * All rights reserved.
 */

#include "std/types.h"

size_t strlen(const char *str)
{
    int len = 0;
    while(str[len] != '\0')
        len++;
    
    return len;
}
