char *strcpy(char *dest, const char *src)
{
    char *dptr;
    const char *sptr;
    
    sptr= src;
    dptr = dest;
    while (*sptr != '\0')
        *dptr++ = *sptr++; 

    return dest;
}
