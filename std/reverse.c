
#include "std/types.h"

void reverse(void *buf, size_t len)
{
    int tmp, i, j;
    char *p;

    j = len; 
    p = (char *) buf;
    for (i = 0; i < j; i++, j--){
        tmp = p[i];
        p[i] = p[j];
        p[j] = tmp;
    }
}
