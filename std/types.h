/* x86_64 types */  
#ifndef types_h
#define types_h

/* os */
typedef int                 pid_t;
typedef unsigned int        uid_t;
typedef unsigned int        gid_t;
typedef long                off_t;
typedef int                 mode_t;

/* general use */
typedef unsigned long       size_t;
typedef long                ssize_t;
typedef unsigned char       byte_t;
typedef signed char         int8_t;
typedef short               int16_t;
typedef int                 int32_t;
typedef int                 int_t;
typedef long                intptr_t;
typedef long                int64_t;
typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned int        uint_t;
typedef unsigned long       uint64_t;
typedef unsigned char       bool_t;      

#endif /* types_h */
