#ifndef malloc_h
#define malloc_h

void *malloc(unsigned long bytes);
void free(void *ptr);

#endif /* malloc_h */
