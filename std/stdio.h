/* 
 * Copyright (c) Grant Murphy 
 * All rights reserved.
 */
#ifndef io_h
#define io_h

void puts(const char *str);

#endif /* io_h */ 
