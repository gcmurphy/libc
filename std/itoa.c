#include "std/string.h"
#include "std/utils.h"
#include "std/types.h"

char* itoa(int n, char *buf, int radix)
{
    char digits[] = "0123456789abcdef";
    int i;
    int x; 
    int negative;
    
    negative = 0;
    if (n < 0){
        negative = 1; 
        n *= -1;
    }

    i = 0;
    while (n){
        x = n % radix; 
        n = n / radix;
        buf[i] = digits[x % sizeof(digits)];
        ++i;
    }

    if (negative && radix == 10){
        buf[i] = '-';
        ++i;
    }

    if (radix == 16){
        buf[i] = 'x'; ++i;
        buf[i] = '0'; ++i;
    }

    buf[i] = '\0';
        
    reverse(buf, strlen(buf)-1);
    return buf;
}
