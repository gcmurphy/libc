/* 
 * Copyright (c) Grant Murphy 
 * All rights reserved.
 */ 
#ifndef unistd_h
#define unistd_h

#include "std/types.h"

/* Null pointer definition */
#define NULL 0

/* File permissions */
#define R_OK 4 /* Read Permission */
#define W_OK 2 /* Write Permission */
#define X_OK 1 /* Execute Permission */
#define F_OK 0 /* Exists */

/* Seek directions */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

/* File locking */
#define F_ULOCK 0 
#define F_LOCK  1
#define F_TLOCK 2
#define F_TEST  3

/* Standard file descriptors */
#define STDIN_FILENO    0 
#define STDOUT_FILENO   1
#define STDERR_FILENO   2

void    quit(int rc); 
int     write(int fd, const void *buf, size_t len);
int     read(int fd, const void *buf, size_t sz);
int     close(int fd);
pid_t   getpid();
int     access(const char *, int);
uint_t  alarm(uint_t);
int     brk(void*);
int     chdir(const char *);
int     chown(const char *, uid_t, gid_t);
int     dup(int);
int     dup2(int, int);
int     execve(const char *, const char *[], const char*[]);
int     fchown(int, uid_t, gid_t);
int     fchdir(int);
int     fdatasync(int);
pid_t   fork(void);
int     fsync(int);
int     ftruncate(int, off_t);
char*   getcwd(char *, size_t);
gid_t   getegid(void);
uid_t   geteuid(void);
gid_t   getgid(void);
int     getgroups(int, gid_t[]);
pid_t   getpgid(pid_t);
pid_t   getpgrp(void);
pid_t   getpid(void);
pid_t   getppid(void);
pid_t   getsid(pid_t);
uid_t   getuid(void);
int     lchown(const char*, uid_t, gid_t);
int     link(const char *, const char*);
off_t   lseek(int, off_t, int);
int     pause(void);
int     pipe(int[2]);
ssize_t pread(int, void *, size_t, off_t);
ssize_t pwrite(int, const void *, size_t, off_t);
int     readlink(const char *, char *, size_t);
int     rmdir(const char*);
void    *sbrk(intptr_t);
int     setgid(gid_t);
int     setpgid(pid_t, pid_t);
int     setregid(gid_t, gid_t);
int     setreuid(uid_t, uid_t);
pid_t   setsid(void);
int     setuid(uid_t);
int     symlink(const char*, const char *);
void    sync(void);
int     truncate(const char*, off_t);
int     unlink(const char*);
pid_t   vfork(void);

/* Omitting */

// chroot
// confstr
// crypt
// ctermid
// cuserid
// encrypt
// execl
// execle
// execlp
// execvp
// _exit (replaced by quit)
// fpathconf (separate)
// gethostid
// getlogin
// getlogin_r
// getopt
// getwd
// isatty
// pathconf (separate)
// pthread_atfork
// sysconf (separate)
// tcgetpgrp
// tcsetpgrp

/* Todo */

// void    *sbrk(intptr_t);
// int     lockf(int,  int, off_t);
// int     setpgrp(void);
// uint_t  sleep(uint_t);

#endif /* unistd_h */


