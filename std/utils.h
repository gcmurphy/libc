#ifndef utils_h
#define utils_h

#include "std/types.h"

void reverse(void *, size_t);
char *base64_encode(void *, size_t);
void clump(int fd, void * data, size_t size, const char *label);

#endif /* utils */
