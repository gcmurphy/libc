
#include "std/stdio.h"
#include "std/stdlib.h"
#include "std/string.h"
#include "std/stdarg.h"
#include "std/unistd.h"
#include "std/utils.h"
#include "std/malloc.h"
#include "std/debug.h"

void debugf(const char *fmt, ...)
{
    va_list list;
    va_start(list, fmt);

    blob_t *raw;
    char *str;
    char *enc;
    int num;
    char buf[64]; 
    const char *p = fmt;
    while (*p != '\0'){
        
        switch(*p){
            case '%':

                ++p;
                memset(buf, 0, sizeof(buf));
            
                switch(*p){

                    case '\0': 
                        break;

                    case 's' :
                        str = va_arg(list, char *);
                        write(1, str, strlen(str)); 
                        break;

                    case 'x' : 
                        num = va_arg(list, int);
                        itoa(num, buf, 16);
                        write(1, buf, strlen(buf));
                        break;

                    case 'd' : 
                        num = va_arg(list, int);
                        itoa(num, buf, 10);
                        write(1, buf, strlen(buf));
                        break;
                     
                    case 'b': 
                        num = va_arg(list, int);
                        itoa(num, buf, 2);
                        write(1, buf, strlen(buf));
                        break;

                    case '=':
                        raw =(blob_t*) va_arg(list, blob_t*);
                        enc = base64_encode((void*)raw->data, raw->length);
                        write(1, enc, strlen(enc));
                        free(enc);
                        break;

                    case '%':
                        write(1, p, 1);
                        break;

                }
                break;

            default: 
                write(1, p, 1); 
        }
        ++p;
    }

    va_end(list);
}

