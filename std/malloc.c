/*
 * malloc impl taken verbatim from 
 * The C Book by K&R.
 *
 */

#include "std/unistd.h"
#include "std/malloc.h"

#define NALLOC 1024         /* minimum #units to request */
typedef long Align;         /* For alignment to long boundary */

union header {              /* Block header */
    struct {
        union header *ptr;  /* Next block if on free list */
        unsigned size;      /* Size of this block */
    } s;
    Align x;                /* Force alignment of blocks */
};
typedef union header Header;

static Header base;             /* Empty list to get started */
static Header *freep = NULL;    /* Start of the free list */ 
static Header *morecore(unsigned long);

/* malloc: general-purpose storage allocator */
void *malloc(unsigned long nbytes)
{
    Header *p, *prevp;
    unsigned nunits;

    nunits = (nbytes+sizeof(Header)-1)/sizeof(Header) + 1;
    if ((prevp = freep) == NULL){
        base.s.ptr = freep = prevp = &base;
        base.s.size = 0;
    }

    for (p = prevp->s.ptr ;; prevp = p, p = p->s.ptr){
        if (p->s.size >= nunits){
            if (p->s.size == nunits){
                prevp->s.ptr = p->s.ptr;
            } else {
                p->s.size -= nunits;
                p += p->s.size;
                p->s.size = nunits;
            }
            freep = prevp;
            return (void*)(p+1);
        }
        if (p == freep){
            if ((p = morecore(nunits)) == NULL){
                return NULL;
            }
        }
    }
}

/* morecore: ask system for more memory */
static Header *morecore(unsigned long nu)
{
    char *cp;
    Header *up;

    if (nu < NALLOC)
        nu = NALLOC;

    cp = sbrk(nu * sizeof(Header));
    if (cp == (char *) -1){ /* no space at all */
        return NULL;
    }
    up = (Header *) cp;
    up->s.size = nu;
    free((void*)(up+1));
    return freep;
}


/* free: put block ap in free list */
void free(void *ap)
{
    Header *bp, *p;

    bp = (Header *) ap-1;  /* point to block header */
    for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr){
        if (p >= p->s.ptr && (bp > p || bp < p->s.ptr)){
            break;
        }
    }

    if (bp + bp->s.size == p->s.ptr){ 
        bp->s.size += p->s.ptr->s.size;
        bp->s.ptr = p->s.ptr->s.ptr;
    } else {
        bp->s.ptr = p->s.ptr->s.ptr;
    }

    if (p + p->s.size == bp){
        p->s.size += bp->s.size;
        p->s.ptr = bp->s.ptr;
    } else{
        p->s.ptr = bp;
    }
    freep = p;
}
