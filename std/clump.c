
#include "std/types.h"
#include "std/unistd.h"
#include "std/string.h"
#include "std/stdlib.h"

static void write_cstr(int fd, const char *str)
{
    write(fd, str, strlen(str)); 
}

static void write_hex(int fd, byte_t b)
{
    char buf[32];
    itoa((int) b, buf, 16);
    write(fd, buf, strlen(buf));
}

static void write_size(int fd, size_t sz)
{
    char buf[32]; 
    itoa((int) sz, buf, 10) ;
    write(fd, buf, strlen(buf));
}

/* TODO: reimpl with streaming rather than all the syscalls */
void clump(int fd, void * data, size_t size, const char *label)
{
    int pos; 
    byte_t *ptr;
    if (size == 0)
        return;

    write_cstr(fd, "const unsigned char ");
    write_cstr(fd, label);
    write_cstr(fd, "[] = { \n\t");

    pos = 0;
    ptr = (byte_t*) data;
    for (pos = 0; pos < size; ++pos){

        write_hex(fd, ptr[pos]);

        if (pos+1 != size)
            write_cstr(fd, ", ");

        if ((pos+1) % 8 == 0)
            write_cstr(fd, "\n\t");

    }
    write_cstr(fd, "\n};\n");
    write_cstr(fd, "size_t ");
    write_cstr(fd, label);
    write_cstr(fd, "_size = ");
    write_size(fd, size);
    write_cstr(fd, ";\n\n");

}

