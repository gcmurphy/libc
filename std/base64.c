/*
 * Copyright (c) Grant Murphy 
 *
 * All rights reserved. 
 */
#include "std/string.h"
#include "std/malloc.h"

static char base64_standard[] = 
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcedfghijklmnopqrstuvwxyz"
    "0123456789+/";

static char base64_standard_pad = '=';

char *base64_encode(void *buf, size_t len)
{
    int pos, out, padding, i;
    byte_t chunk[3];
    byte_t *p; 
    char *encoding;
    
    padding = 0;
    p = (byte_t*) buf;
    pos = 0, out = 0;
    encoding = (char*) malloc(((len *4)/3) + 4); /* generous */

    while (pos < len && padding == 0) {

        padding = 0;
        memset(chunk, 0, sizeof(chunk));
        for (i = 0; i < sizeof(chunk); ++i) {
            if (pos < len) 
                chunk[i] = p[pos++];
            else
                padding++;
        }

        uint32_t group = (chunk[0] << 16)
                       + (chunk[1] << 8 ) 
                       + (chunk[2]      ); 

        encoding[out++] = base64_standard[(group >> 18) & 0x3f];
        encoding[out++] = base64_standard[(group >> 12) & 0x3f];

        if (padding <= 1)
            encoding[out++] = base64_standard[(group >> 6) & 0x3f];

        if (! padding)
            encoding[out++] = base64_standard[group & 0x3f];
    }
    
    if (padding){
        memset((encoding+out), base64_standard_pad, padding);
    }
    encoding[out+padding] = '\0';
    return encoding;
}


