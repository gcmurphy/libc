typedef enum 
{
    ALIGN_16, 
    ALIGN_24, 
    ALIGN_32, 
    ALIGN_64
} 
align_t;

int alignment(void *p)
{
     if ((((unsigned long)p) & 15) == 0)
        return ALIGN_16;

    if ((((unsigned long)p) & 23) == 0)
        return ALIGN_24;

    if ((((unsigned long)p) & 31) == 0)
        return ALIGN_32;

    if ((((unsigned long)p) & 63) == 0)
        return ALIGN_64;

    return -1;
}
