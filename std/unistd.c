/* 
 * Copyright (c) Grant Murphy 
 * All rights reserved.
 */
#include "std/types.h"
#include "os/syscall.h"

int write(int fd, const void *buf, size_t len){
    return syscall(syscall_write, fd, buf, len); 
}

int read(int fd, const void *buf, size_t sz){
    return syscall(syscall_read, fd, buf, sz); 
}

int close(int fd){
    return syscall(syscall_close, fd);
}

/* _exit() noreturn warning */
void quit(int rc){
    syscall(syscall_exit, rc);
}

pid_t getpid(void){
    return syscall(syscall_getpid);
}

int access(const char *path, int mode){
    return syscall(syscall_access, path, mode);
}

uint_t alarm(uint_t seconds){
    return syscall(syscall_alarm, seconds);
}

int brk(void* ptr){
    return syscall(syscall_brk, ptr);
}

int chdir(const char *dir){
    return syscall(syscall_chdir, dir);
}

int chown(const char *path, uid_t user, gid_t grp){
    return syscall(syscall_chown, path, user, grp); 
}

int dup(int fd){
    return syscall(syscall_dup, fd);
}

int dup2(int fd1, int fd2){
    return syscall(syscall_dup2, fd1, fd2);
}

int execve(const char *file, const char *argv[], const char *envp[]){
    return syscall(syscall_execve, file, argv, envp);
}

int fchown(int fd, uid_t uid, gid_t gid){
    return syscall(syscall_fchown, fd, uid, gid);
}

int fchdir(int fd){
    return syscall(syscall_fchdir, fd);
}

int fdatasync(int fd){
    return syscall(syscall_fdatasync, fd);
}

pid_t fork(){
    return syscall(syscall_fork);
}

int fsync(int fd){
    return syscall(syscall_fsync);
}

int ftruncate(int fd, off_t off){
    return syscall(syscall_ftruncate);
}

char* getcwd(char *buf, size_t sz){
    syscall(syscall_getcwd, buf, sz);
    return buf;
}

gid_t getegid(){
    return syscall(syscall_getegid);
}

uid_t geteuid(){
    return syscall(syscall_geteuid);
}

gid_t getgid(){
    return syscall(syscall_getgid);
}

int getgroups(int gidsetsize, gid_t grouplist[]){
    return syscall(syscall_getgroups, gidsetsize, grouplist);
}

pid_t getpgid(pid_t pid){
    return syscall(syscall_getpgid, pid);    
}

pid_t getpgrp(){
    return syscall(syscall_getpgrp);
}

pid_t getppid(){
    return syscall(syscall_getppid);
}

pid_t getsid(pid_t pid){
    return syscall(syscall_getsid, pid);
}

uid_t getuid(){
    return syscall(syscall_getuid);
}

int lchown(const char* path, uid_t uid, gid_t gid){
    return syscall(syscall_lchown, path, uid, gid);
}

int link(const char* path1, const char* path2){
    return syscall(syscall_link, path1, path2);
}

/* TODO: Need to use fcntl to lock file. There is a 
 * flock system call but this has different semantics.
int lockf(int fd, int cmd, off_t len){
    return syscall(syscall_lockf, fd, cmd, len);
}
*/

off_t lseek(int fd, off_t offset, int direction){
    return syscall(syscall_lseek, fd, offset, direction); 
}

int pause(){
    return syscall(syscall_pause);
}

int pipe(int fd[2]){
    return syscall(syscall_pipe, fd);
}

ssize_t pread(int fd, void *buf, size_t nbyte, off_t offset){
    return syscall(syscall_pread64, fd, buf, nbyte, offset);
}

ssize_t pwrite(int fd, const void *buf, size_t nbyte, off_t offset){
    return syscall(syscall_pwrite64, fd, buf, nbyte, offset);
}

int readlink(const char *path, char *buf, size_t bufsz){
    return syscall(syscall_readlink, path, buf, bufsz);
}

int rmdir(const char* dir){
    return syscall(syscall_rmdir, dir);
}

void *sbrk(intptr_t inc)
{
    uint64_t cur = brk(0);
	if (inc && syscall(syscall_brk, cur+inc) != cur+inc) {
        return (void *)-1;
    }
	return (void *)cur;
}

int setgid(gid_t gid){
    return syscall(syscall_setgid, gid);    
}

int setpgid(pid_t pid, pid_t pgid){
    return syscall(syscall_setpgid, pid, pgid);
}

/* TODO: Figure out how to implement this 
int setpgrp(){
    return syscall(syscall_setpgrp);
}
*/

int setregid(gid_t rgid, gid_t egid){
    return syscall(syscall_setregid, rgid, egid);
}

int setreuid(uid_t ruid, uid_t euid){
    return syscall(syscall_setreuid, ruid, euid);
}

pid_t setsid(){
    return syscall(syscall_setsid); 
}

int setuid(uid_t uid){
    return syscall(syscall_setuid);
}

/* TODO: Implement this using nanosleep? 
uint_t  sleep(uint_t s){
    return syscall(syscall_sleep, s);
}
*/

int symlink(const char* path1, const char* path2){
    return syscall(syscall_symlink, path1, path2);    
}

void sync(){
    syscall(syscall_sync);
}

int truncate(const char* path, off_t len){
    return syscall(syscall_truncate, path, len);    
}

int  unlink(const char* path){
    return syscall(syscall_unlink, path);
}

pid_t vfork(){
    return syscall(syscall_vfork);
}

